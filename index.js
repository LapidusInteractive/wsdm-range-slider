import { event, select } from "d3-selection"
import { scaleLinear } from "d3-scale"
import { axisBottom } from "d3-axis"
import { brushX } from "d3-brush"
import { niceNum } from "wsdm-utils"

// Default options for the slider
const defaults = {
  // nice default for the slider's spectrum
  domain: [ 0, 1 ],

  // spacing around the slider
  margin: { top: 0, right: 5, bottom: 0, left: 5 },

  // show tiny tooltips with the range values that follow the handles?
  showTips: false,

  // if the range values are shown, is it on top or below the slider
  tipsPosition: "bottom",

  // formatting for the values returned in the callback
  valueFormat: v => v,

  // formatting for the value displayed in the tooltips
  tipsFormat: v => niceNum(v),

  // formatting for the ticks in the axis
  tickFormat: v => niceNum(v),
}

const slider = () => {
  let slider
  let element
  let config
  let scale
  let brush
  let svg
  let xAxis
  let xAxisContainer
  let previousRange = []
  let sliderId

  /**
   * Set the element, setup the config
   * initialise the slider elements
   * @param  {Object} element DOM Node
   * @param  {Object} config  Optional configuration
   * @return {Void}
   */
  const create = (el, options = {}) => {
    element = getElement(el)
    config = getConfig(options)

    const { width, height, margin } = config

    sliderId = `wsdm-range-slider-${+(new Date())}`

    scale = scaleLinear()
      .range([ 0, width ])
      .clamp(true)

    brush = brushX()
      .extent([ [ 0, 0 ], [ width, 10 ] ])
      .on("brush end", onBrush)

    svg = select(element)
      .style("position", "relative")
    .append("svg")
      .style("display", "block")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)

    slider = svg.append("g")
      .attr("class", "wsdm-range-slider")
      .attr("transform", `translate(${margin.left},${height / 2})`)

    xAxis = axisBottom()
      .scale(scale)
      .tickFormat(config.tickFormat)

    xAxisContainer = slider.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0,10)")
      .call(xAxis)

    select(window).on(`resize.${sliderId}`, resize)

    update()
  }

  /**
   * Resize handler for making the slider responsive
   * @return {Void}
   */
  const resize = () => {
    config.width =
      element.offsetWidth - config.margin.left - config.margin.right

    const { width, height, margin } = config

    scale.range([ 0, width ])
    svg.attr("width", width + margin.left + margin.right)
    slider.attr("transform", `translate(${margin.left},${height / 2})`)
    xAxis.scale(scale)
    xAxisContainer.call(xAxis)
    update()
  }

  /**
   * Clean up after destroying the slider
   * remove the elements, remove resize event handler
   * @return {Void}
   */
  const destroy = () => {
    while (element.firstChild) element.removeChild(element.firstChild)
    select(window).on(`resize.${sliderId}`, null)
  }

  /**
   * Set config, merge defaults with passed options
   * @param {Void} config
   */
  const getConfig = options => {
    if (options === null || typeof options !== "object")
      throw new Error("Options must be an object")

    const { margin } = defaults
    defaults.width = element.offsetWidth - margin.left - margin.right
    defaults.height = element.offsetHeight - margin.top - margin.bottom

    return Object.assign({}, defaults, options)
  }

  /**
   * Check if options passed to update function are valid
   * @param  {Object} config
   * @return {Void}
   */
  const validateConfig = config => {
    if (typeof config.domain !== "undefined" && !Array.isArray(config.domain))
      throw new Error("Domain must be an array.")

    if (typeof config.range !== "undefined" && !Array.isArray(config.range))
      throw new Error("Range must be an array.")
  }

  /**
   * Update the slider with new domain
   * @param  {Object} options new options for the slider
   * @return {Void}
   */
  const update = (options = {}) => {
    Object.assign(config, options)

    validateConfig(config)

    if (typeof config.range === "undefined")
      config.range = config.domain.slice()

    scale
      .domain(config.domain)

    xAxisContainer
      .call(xAxis)
      .selectAll("path, line")
      .attr("stroke", "transparent")

    if (config.showTips) drawTips(config.range)

    slider
      .call(brush)
      .call(brush.move, config.range.map(scale))
  }

  const onBrush = () => {
    const { showTips, onChange, valueFormat } = config
    const s = event.selection || scale.range()
    config.range = s.map(scale.invert, scale)

    if (config.range[0] === previousRange[0] &&
        config.range[1] === previousRange[1]) return

    if (showTips) drawTips(config.range)
    if (onChange) onChange(config.range.map(valueFormat))
    previousRange = config.range.slice()
  }

  const drawTips = data => {
    const { tipsFormat, margin, tipsPosition } = config

    const rangeTips = select(element)
      .selectAll(".wsdm-range-slider-range-tip")
      .data(data)

    rangeTips.enter().append("div")
      .attr("class", "wsdm-range-slider-range-tip")
      .style("position", "absolute")
      .style(tipsPosition, 0)
      .style("transform", "translateX(-50%)")
      .text(d => tipsFormat(d))
      .style("left", d => `${scale(d) + margin.left}px`)
    .merge(rangeTips)
      .text(d => tipsFormat(d))
      .style("left", d => `${scale(d) + margin.left}px`)
  }

  /**
   * Element getter
   * ensure the proper DOM node for the slider exists
   * @param  {Object} element DOM Node
   * @return {Void}
   */
  const getElement = el => {
    if (!el) throw new Error("DOM Element is required")
    if (el instanceof HTMLElement) return el
    throw new Error("Element must be a DOM Node")
  }

  return {
    create,
    update,
    destroy,
  }
}

export default slider
