
# wsdm-range-slider

**NOTE: In active development**

Responsive range slider component for d3 charts, based on d3 brush. Has very simple usage, as demonstrated in the [example](https://lapidusinteractive.gitlab.io/wsdm-range-slider).

# Methods

Slider exposes 3 methods:

| Method      | Arguments  | Description  |
| ----------- | ---------- | ------------ |
| `create` | `(holder::DOMNode, config::Object)` | `holder` is required, inside it the slider is created with the optional `config` |
| `update` | `(config::Object)` | Updates the slider with the new `config` |
| `destroy` | `()` | Destroys the slider, cleans up the DOM and removes the `resize` even handler |

As you can see these 3 methods match nicely lifecycle methods from React, which makes it easy to integrate it into a React component:

- `componentDidMount => slider.create`
- `componentDidUpdate => slider.update`
- `componentWillUnmount => slider.destroy`

# Options
| Property      | Type          | Default  | Description  |
| ------------- | ------------- | -------- | ------------ |
| `domain` | `array` | `[ 0, 1 ]` | The whole spectrum from which user can create the range selection |
| `range` | `array` | `undefined` | The range to which the slider should be set |
| `margin` | `object` | `{ top: 0, right: 5, bottom: 0, left: 5 }` | Spacing around the slider. Default setting gives just enough space to render the handles and outside ticks. |
| `showTips` | `boolean` | `false` | Set to `true` if you want tiny tooltips with the values that follow the handles  |
| `tipsPosition` | `string` | ` "bottom"` | Determines where the tooltips are placed relative to the handles, if shown. Two options are
possible `"top"` and `"bottom"` |
| `onChange` | `function` | `undefined` | Callback firing when user is interacting with the slider, has the current range array as the parameter. |
| `valueFormat` | `function` | `v => v` | By default no formatting is applied to the values produced by the slider. Use it for example if you want to have nicely rounded values, e.g. by passing `Math.round` to it. |
| `tipsFormat` | `function` | `v => niceNum(v)` | Used to display format the values displayed in the tooltips, if shown. By default it will use internal method to produce nicely formatted numbers (1000000 => 1m) |
| `tickFormat` | `function` | `v => niceNum(v)` | Passed to d3 axis tick formatting. By default it will use internal method to produce nicely formatted numbers (1000000 => 1m). Simple trick to remove the ticks completely `() => ""` |

Run development server with:
```
$ npm i
$ npm start
```

Run tests with:

```
$ npm test
```

Build with:

```
$ npm run build
```

Example styling is included in
[example/slider.css](https://gitlab.com/LapidusInteractive/wsdm-range-slider/blob/master/example/slider.css)

[Lapidus Interactive](http://lapidus.se)
