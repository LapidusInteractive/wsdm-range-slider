import wsdmRangeSlider from ".."

const updateResult = (range, id) =>
  document.querySelector(`#${id}`).textContent = `[ ${range.join(", ")} ]`

const slider1 = wsdmRangeSlider()
slider1.create(document.querySelector("#slider1"), {
  onChange: value => updateResult(value, "result1"),
  valueFormat: Math.round,
  domain: [ -100, 100 ],
  margin: { top: 0, right: 10, bottom: 0, left: 10 },
  range: [ -50, 50 ],
})

const slider2 = wsdmRangeSlider()
slider2.create(document.querySelector("#slider2"), {
  onChange: value => updateResult(value, "result2"),
  domain: [ -1, 1 ],
  range: [ -0.5, 0.5 ],
  valueFormat: v => v.toFixed(2),
  tickFormat: d => [ -1, 1 ].indexOf(d) > -1 ? d : "",
  showTips: true,
})
