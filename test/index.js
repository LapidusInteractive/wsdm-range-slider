import test from "tape"

import wsdmRangeSlider from "../index"

const holder = document.createElement("div")
document.body.appendChild(holder)

test("imported component", t => {
  t.plan(1)
  t.is(typeof wsdmRangeSlider, "function", "is a function")
})

test("slider", t => {
  const slider = wsdmRangeSlider()

  t.plan(4)
  t.is(typeof slider, "object", "is an object")
  t.is(typeof slider.create, "function", "has `create` method")
  t.is(typeof slider.update, "function", "has `update` method")
  t.is(typeof slider.destroy, "function", "has `destroy` method")
})

test("`destroy` method", t => {
  const slider = wsdmRangeSlider()

  t.plan(1)
  slider.create(holder)
  slider.destroy()
  t.is(holder.querySelectorAll("*").length,  0, "cleans up the DOM")
})

test("`create` method", t => {
  const slider = wsdmRangeSlider()

  t.plan(6)

  t.throws(
    slider.create,
    undefined,
    "throws when DOM Node is missing"
  )

  t.throws(
    slider.create.bind(null, ""),
    undefined,
    "throws when DOM Node is invalid"
  )

  t.doesNotThrow(
    slider.create.bind(null, holder, {}),
    undefined,
    "does not throw when DOM node is valid"
  )
  slider.destroy()

  t.throws(
    slider.create.bind(null, holder, null),
    undefined,
    "throws when options are not an object"
  )

  t.doesNotThrow(
    slider.create.bind(null, holder),
    undefined,
    "does not throw when options are missing"
  )
  slider.destroy()

  t.doesNotThrow(
    slider.create.bind(null, holder, {}),
    undefined,
    "does not throw when options are an object"
  )
  slider.destroy()
})

test("`update` method", t => {
  const slider = wsdmRangeSlider()
  const domain = [ 0, 5 ]
  const range = [ 2, 2 ]

  t.plan(4)
  slider.create(holder)

  t.throws(
    slider.update.bind(null, { domain: "" }),
    undefined,
    "throws when `domain` is not an array"
  )

  t.doesNotThrow(
    slider.update.bind(null, { domain }),
    undefined,
    "does not throw when `domain` array is passed"
  )

  t.throws(
    slider.update.bind(null, { range: "" }),
    undefined,
    "throws when `range` is not an array"
  )

  t.doesNotThrow(
    slider.update.bind(null, { range }),
    undefined,
    "does not throw when `range` array is passed"
  )

  slider.destroy()
})

test("`create` calls `update`", t => {
  const slider = wsdmRangeSlider()

  t.plan(1)
  slider.create(holder, { domain: [ -5, 5 ] })
  const ticks = [].slice.call(holder.querySelectorAll(".axis text"))
    .map(n => +n.textContent)

  t.is(
    ticks[0],
    -5,
    "when domain is passed in the config"
  )
  slider.destroy()
})

test("slider creates proper DOM", t => {
  const slider = wsdmRangeSlider()
  const type = (s) => typeof holder.querySelector(s)

  slider.create(holder)
  t.plan(6)

  t.is(type("svg"), "object", "SVG")
  t.is(type(".axis"), "object", "Axis")
  t.is(type(".overlay"), "object", "Overlay")
  t.is(type(".selection"), "object", "Selection")
  t.is(type(".handle--e"), "object", "Left Handle")
  t.is(type(".handle--w"), "object", "Right Handle")
  slider.destroy()
})

test("slider handles displaying the range tooltips", t => {
  const slider = wsdmRangeSlider()
  t.plan(4)

  slider.create(holder)
  t.is(
    holder.querySelectorAll(".wsdm-range-slider-range-tip").length,
    0,
    "doesn't display them by default"
  )
  slider.destroy()

  slider.create(holder, { showTips: true })
  t.is(
    holder.querySelectorAll(".wsdm-range-slider-range-tip").length,
    2,
    "displays them when turned on"
  )
  t.is(
    holder.querySelector(".wsdm-range-slider-range-tip").style.bottom,
    "0px",
    "displays them on bottom by default"
  )
  slider.destroy()

  slider.create(holder, { showTips: true, tipsPosition: "top" })
  t.is(
    holder.querySelector(".wsdm-range-slider-range-tip").style.top,
    "0px",
    "displays them on top when set to `top`"
  )
  slider.destroy()
})
